require 'rails_helper'

RSpec.describe Discussion, type: :model do
  let(:post){ stub_model(Post) }

  describe 'validations' do
    it 'requires a body' do
      d = Discussion.create(:post => post)
      expect(d.errors[:body]).to include('Must include body')
    end

    it 'requires a parent post id' do
      d = Discussion.create(:body => 'body', :post => nil)
      expect(d.errors.messages[:post]).to include('must exist')
    end

    it 'creates if it has a body and a parent' do
      d = Discussion.create(:body => 'body', :post => post)
      expect(d.errors).to be_empty
    end
  end

  describe 'associations' do
    it 'a discussion can be created as a child named folllowups' do
      d1 = Discussion.create(:body => 'body1', :post => post)
      d2 = d1.followups.create(:body => 'body1-2', :post => d1.post)
      expect(d2.errors).to be_empty
      expect(d1.followups.first).to eq(d2)
      expect(d1.followups.first.parent_id).to eq(d1.id)
    end

    it 'a discussion can be created as itself' do
      d1 = Discussion.create(:body => 'body1', :post => post)
      expect(d1.parent_id).to be_nil
    end

    it 'a followup discussion must have a post' do
      d1 = Discussion.create(:body => 'body1', :post => post)
      d2 = d1.followups.create(:body => 'body1-2')
      expect(d2.errors[:post]).to include('must exist')
    end
  end

  describe 'scopes' do
    context :parent_discussions do
      it 'should return all discussions that are the parent discussion' do
        discussions = FactoryGirl.create_list(:discussion, 5)
        discussions.each do |parent_discussion|
          FactoryGirl.create( :followup,
                              :post => parent_discussion.post,
                              :parent_id => parent_discussion.id )
        end
        result = Discussion.all.parent_discussions
        result.each do |r|
          expect(r.parent_id).to be_nil
        end
      end

      it 'should return an empty list if there are no parent discussions' do
        result = Discussion.all.parent_discussions
        expect(result).to eq([])
      end
    end

    context :recently_created do

      it 'has a recently_created scope' do
        expect { Discussion.all.recently_created }.to_not raise_error(NoMethodError)
      end

      it 'returns the recently created posts first' do
        dates =    ['2017-01-01', '2017-01-22', '2017-05-01']
        expected = ['2017-05-01', '2017-01-22', '2017-01-01', ]
        discussions = FactoryGirl.create_list(:discussion, 3)
        discussions.each_with_index do |discussion, index|
          discussion.update_attributes(:created_at => dates[index])
        end

        result = Discussion.all.recently_created.map{ |d| d.created_at.strftime('%Y-%m-%d') }
        expect(result).to eq(expected)
      end

      it 'should return the most recent followup discussions' do
        parent_discussion = FactoryGirl.create(:discussion)
        dates =    ['2017-01-01', '2017-01-05', '2017-01-02']
        expected = ['2017-01-05', '2017-01-02', '2017-01-01']

        followups = FactoryGirl.create_list( :followup, 3,
                                             :post => parent_discussion.post,
                                             :parent => parent_discussion )
        followups.each_with_index do |f, i|
          f.update_attributes(:created_at => dates[i])
        end

        result = parent_discussion.followups.recently_created.map{ |d| d.created_at.strftime('%Y-%m-%d') }
        expect(result).to eq(expected)
      end

      it 'should return empty list if no discussions are found' do
        expect(Discussion.all.recently_created).to eq([])
      end
    end
  end
end
