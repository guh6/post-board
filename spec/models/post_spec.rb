require 'rails_helper'

RSpec.describe Post, type: :model do

  describe 'associations' do
    it 'can contain many discussions' do
      post = Post.create(:title => 'title', :body => 'body', :priority => 1)
      allow(post).to receive(:discussions).and_return([1,2,3])

      expect(post.discussions).to eq([1,2,3])
    end
  end

  describe 'validations' do
    it 'must contain a title' do
      post = Post.create(:body => 'text body', :priority => 'text type')
      expect(post.errors[:title]).to include('Must provide a title')
    end

    it 'must contain a body' do
      post = Post.create(:title => 'text title', :priority => 1)
      expect(post.errors[:body]).to include('Must provide a body')
    end

    it 'must contain a priority' do
      post = Post.create(:body => 'text body', :title => 'title')
      expect(post.errors[:priority]).to include('Must provide a priority')
    end

    it 'must contain a title, body and priority' do
      post = Post.create(:title => 'title', :body => 'body', :priority => 1)
      expect(post.errors).to be_empty
    end
  end

  describe 'scopes' do
    context :recently_updated do

      it 'has a recently_updated scope' do
        expect{ Post.all.recently_updated }.to_not raise_error(NoMethodError)
      end

      it 'returns the posts with the most recently updated at first' do
        post_updated_at = ['2011-01-01', '2013-01-01', '2010-01-01']
        expected_posts =  ['2013-01-01', '2011-01-01', '2010-01-01']
        posts = FactoryGirl.create_list(:post, 3)

        posts.each_with_index do |p, i|
          p.update_attributes(:updated_at => post_updated_at[i])
        end

        results = Post.all.recently_updated.pluck(:updated_at).map{ |date| date.strftime('%Y-%m-%d') }
        expect(results).to eq(expected_posts)
      end
    end
  end

  describe 'enum' do
    it 'low priority should be zero' do
      post = FactoryGirl.create(:post, :priority => 0)
      expect(post.priority).to eq('low')
    end

    it 'normal priority should be one' do
      post = FactoryGirl.create(:post, :priority => 1)
      expect(post.priority).to eq('normal')
    end

    it 'high priority should be two' do
      post = FactoryGirl.create(:post, :priority => 2)
      expect(post.priority).to eq('high')
    end
  end

end
