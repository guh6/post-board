require 'rails_helper'

RSpec.describe DiscussionsController, type: :controller do

  context 'index' do
    it 'has an index action that response to js' do
      allow(Discussion).to receive(:find_by_post_id).and_return([])
      get :index, params: {:post_id => 1}, :xhr => true

      expect(response.status).to eq(204)
    end

    it 'redirects to main page for http requests' do
      get :index, params: {:post_id => 1}, :xhr => false
      expect(response).to have_http_status(302)
      expect(response).to redirect_to(posts_path)
    end

    it 'returns 204 for no content' do
      allow(Discussion).to receive_message_chain(:parent_discussions, :where).and_return([])
      get :index, params: {:post_id => 1}, :xhr => true

      expect(response.status).to eq(204)
    end

    it 'returns 204 if no discussions are found' do
      allow(Discussion).to receive_message_chain(:parent_discussions, :where).and_return(nil)
      get :index, params: {:post_id => 1}, :xhr => true

      expect(response.status).to eq(204)
    end

    it 'returns 200 for good response' do
      discussion = instance_double(Discussion, :followups => [], :to_json => true)
      allow(Discussion).to receive_message_chain(:parent_discussions, :where).and_return([discussion])
      get :index, params: {:post_id => 1}, :xhr => true

      expect(response.status).to eq(200)
    end

    it 'returns json data as its content type' do
      discussion = instance_double(Discussion, :followups => [], :to_json => true)
      allow(Discussion).to receive_message_chain(:parent_discussions, :where).and_return([discussion])
      get :index, params: {:post_id => 1}, :xhr => true

      expect(response.content_type).to include("json")
    end

    it 'returns 500 for bad server error' do
      allow(Discussion).to receive_message_chain(:parent_discussions, :where){raise RuntimeError}
      get :index, params: {:post_id => 1}, :xhr => true

      expect(response.status).to eq(500)
    end

    describe 'discussions outcome' do
      it 'returns discussion followed by its child discussions as a hash array' do
        # Setting up test data with one post having 3 discussions and the 2nd and 3rd
        # * discussions each having 2 and 3 followups, respectively.
        post = FactoryGirl.create(:post)
        discussions = FactoryGirl.create_list(:discussion, 3, :post => post)
        FactoryGirl.create_list(:followup, 2, :post => post, :parent => discussions[1])
        FactoryGirl.create_list(:followup, 3, :post => post, :parent => discussions[1])

        expected = {}
        post.discussions.parent_discussions.each do |parent|
          expected[parent.to_json] = parent.followups.to_json
        end

        get :index, params: {:post_id => 1}, :xhr => true
        expect(response.body).to eq(expected.to_json)
      end
    end
  end
end
