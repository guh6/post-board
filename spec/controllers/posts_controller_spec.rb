require 'rails_helper'

RSpec.describe PostsController, type: :controller do

  context 'index' do
    it 'renders the index view' do
      get :index
      expect(response).to render_template('index')
    end

    it '200 status code' do
      get :index
      expect(response).to have_http_status(200)
    end

    it 'retrieves all of the posts' do
      expected = [instance_double(Post)]
      allow(Post).to receive(:all).and_return(expected)
      get :index
      expect(assigns(:posts)).to eq(expected)
    end
  end

  context 'create' do
    context 'request types' do
      it 'has a create action' do
        expect{ post :create }.to_not raise_error(AbstractController::ActionNotFound)
      end
    end

    context 'valid request' do
      let(:valid_request){
        {
          :title => 'some title',
          :body => 'some body',
          :priority => :low
        }
      }

      it 'returns 201' do
        post :create, :params => { post: valid_request }
        expect(response).to have_http_status(201)
      end

      it 'returns the created object as JSON for valid request' do
        post :create, :params => { post: valid_request }
        expect(response.body).to include('"title":"some title"') &
                                 include('"body":"some body"') &
                                 include('"priority":"low"')
      end

      it 'sets the content type as application/json' do
        post :create, :params => { post: valid_request }
        expect(response.content_type).to eq('application/json')
      end
    end

    context 'bad request' do
      it 'returns a 400 for bad request for missing params' do
        post :create, :params => { post: { :body => 'body'}}
        expect(response).to have_http_status(400)
      end

      it 'returns model validation errors for valid params but failed validations' do
        post :create, :params => { post: { :title=> '', :body => '', :priority => nil}}
        expect(response.body).to include('"message":"Bad Request"') &
                                 include('"title":"Must provide a title"') &
                                 include('"body":"Must provide a body"') &
                                 include('"priority":"Must provide a priority"')
      end

      it 'sets the content type as application/json' do
        post :create, :params => { post: { :title=> '', :body => '', :priority => nil}}
        expect(response.content_type).to eq('application/json')
      end
    end

    context 'server error' do
      it 'returns 500 for server error' do
        allow(Post).to receive(:create){raise RuntimeError.new('Template message')}
        post :create, :params => { post: {:title=> 'title', :body => 'body', :priority => :low }}
        expect(response).to have_http_status(500)
      end

      it 'explains loosely what went wrong' do
        allow(Post).to receive(:create){raise RuntimeError.new('Template message')}
        post :create, :params => { post: {:title=> 'title', :body => 'body', :priority => :low }}
        expect(response.body).to include('Error with server. Cannot create post.') &
                                 include('Template message')
      end
    end
  end

  context 'update' do
    before(:each) do
      @post = FactoryGirl.create(:post)
    end

    def updated_attributes
      {
        :title => @post.title,
        :body => @post.body,
        :priority => @post.priority
      }
    end

    describe 'request' do
      it 'has an update action through patch' do
        expect{ patch :update, :params => {:id => @post.id} }.to_not raise_error(AbstractController::ActionNotFound)
      end
    end

    describe 'succesful update' do
      before(:each) { patch :update, :params => {:id => @post.id, :post => updated_attributes} }

      it 'returns 200 for successful update' do
        expect(response).to have_http_status(200)
      end

      it 'returns the updated object via json' do
        expect(response.body).to eq(@post.to_json)
      end

      it 'returns application/json content type' do
        expect(response.content_type).to eq('application/json')
      end
    end

    describe 'invalid update' do
      before(:each) { patch :update, :params => {:id => @post.id, :post => {body: '', title: 'abc'}} }

      it 'returns 412 - no content for bad updates' do
        expect(response).to have_http_status(412)
      end

      it 'returns 412 for record not found' do
        allow(Post).to receive(:find).with(@post.id.to_s){raise ActiveRecord::RecordNotFound}
        expect(response).to have_http_status(412)
      end

      it 'returns a message for the bad update' do
        expect(response.body).to include("\"message\":\"Bad Request for updating\"")
      end

      it 'returns the errors along with the message' do
        expect(response.body).to include("\"body\":\"Must provide a body\"")
      end

      it 'returns application/json content type' do
        expect(response.content_type).to eq('application/json')
      end

    end

    describe 'error' do
      before(:each) { allow(Post).to receive(:find){ raise RuntimeError } }

      it 'returns 500 for server error (catches all exception)' do
        patch :update, :params => {:id => @post.id, :post => updated_attributes}
        expect(response).to have_http_status(500)
      end

      it 'returns friendly message' do
        patch :update, :params => {:id => @post.id, :post => updated_attributes}
        expect(response.body).to include('Server error occurred.')
      end
    end

    describe 'behavior' do
      it 'updates the title' do
          patch :update, :params => {:id => @post.id, :post => { :title => "New TITLE!"}}
          expect do
            @post.reload
          end.to change(@post, :title).to('New TITLE!')
      end

      it 'updates the body' do
        patch :update, :params => {:id => @post.id, :post => { :body => "New BODY!"}}
        expect do
          @post.reload
        end.to change(@post, :body).to('New BODY!')
      end

      it 'updates the priority' do
        patch :update, :params => {:id => @post.id, :post => { :priority => :normal}}
        expect do
          @post.reload
        end.to change(@post, :priority).to("normal")
      end
    end
  end

  context 'destroy' do
    before(:each){ @post = FactoryGirl.create(:post) }

    describe 'request' do
      it 'has a destroy action' do
        expect do
          delete :destroy, :params => {:id => @post.id}
        end.to_not raise_error(AbstractController::ActionNotFound)
      end
    end

    describe 'good request' do
      it 'returns 200' do
        delete :destroy, :params => {:id => @post.id}
        expect(response).to have_http_status(200)
      end

      it 'returns succcess message that resource is deleted' do
        delete :destroy, :params => {:id => @post.id}
        expect(response.body).to include('Record deleted')
      end

      it 'returns the message as text/plain' do
        delete :destroy, :params => {:id => @post.id}
        expect(response.content_type).to eq('text/plain')
      end
    end

    describe 'bad request' do

      it 'returns 412' do
        allow(Post).to receive(:find).with(@post.id.to_s).and_return(@post)
        allow(@post).to receive(:destroyed?).and_return(false)

        delete :destroy, :params => {:id => @post.id}
        expect(response).to have_http_status(412)
      end

      it 'returns a message' do
        allow(Post).to receive(:find).with(@post.id.to_s).and_return(@post)
        allow(@post).to receive(:destroyed?).and_return(false)

        delete :destroy, :params => {:id => @post.id}
        expect(response.body).to eq('Cannot delete')
      end

      it 'returns 412 for record not found' do
        allow(Post).to receive(:find).with(@post.id.to_s){ raise ActiveRecord::RecordNotFound }

        delete :destroy, :params => {:id => @post.id}
        expect(response).to have_http_status(412)
      end

      it 'returns 412 and message for record not found' do
        allow(Post).to receive(:find).with(@post.id.to_s){ raise ActiveRecord::RecordNotFound }

        delete :destroy, :params => {:id => @post.id}
        expect(response.body).to eq('Not found')
      end

      it 'returns the message as text/plain' do
        delete :destroy, :params => {:id => @post.id}
        expect(response.content_type).to eq('text/plain')
      end
    end

    describe 'errors' do
      before(:each){ allow(Post).to receive(:find).with(@post.id.to_s){ raise RuntimeError } }

      it 'returns 500' do
        delete :destroy, :params => {:id => @post.id}
        expect(response).to have_http_status(500)
      end

      it 'returns message' do
        delete :destroy, :params => {:id => @post.id}
        expect(response.body).to include('Server error')
      end
    end

    describe 'behavior' do
      it 'deletes given id' do
        expect(@post).to_not be_nil
        delete :destroy, :params => {:id => @post.id}
        expect(response).to have_http_status(200)
        expect{ @post.reload }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end
end
