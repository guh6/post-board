RSpec.configure do |config|
  config.include FactoryGirl::Syntax::Methods
end

FactoryGirl.define do
  factory :post do
    title "post title"
    body "something is here"
    priority :low
  end

  factory :discussion do
    association :post, :factory => :post
    body "sample body"
    parent_id nil
  end

  factory :followup, :class => Discussion do
    association :post, :factory => :post
    association :parent, :factory => :discussion
    body "sample followup body"
  end
end
