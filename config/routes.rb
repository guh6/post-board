Rails.application.routes.draw do
  resources :posts do
    resources :discussions
  end
  resources :records

  get '/', :to => 'records#index'
end
