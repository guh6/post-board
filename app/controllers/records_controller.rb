class RecordsController < ApplicationController
  def index
    @records = Record.all
    Rails.logger.info "Records found: #{@records.size}"
  end

  def create
    respond_to do |format|
      format.js do
        @record= Record.create(record_params)
        if @record.valid?
          puts 'Sending back 200'
          render :json => @record.to_json, :status => 200
        else
          puts 'Sending back 400'
          puts @record.errors.to_json
          render :json => @record.errors.to_json, :status => 400
        end
      end
    end
  end

  def destroy
    respond_to do |format|
      format.js do
        begin
          @record = Record.find_by_id(params[:id])
          @record.destroy
          render :plain => "Record deleted", :status => 204
        rescue Exception => e
          Rails.logger.warn "Unable to delete record"
          render :plain => "Unable to delete record", :status => 500
        end
      end
    end
  end

  def update
    respond_to do |format|
      format.js do
        begin
          @record = Record.find_by_id(params[:id])
          if(@record.update_attributes(record_params))
            render :json => @record.to_json, :status => 200
          else
            render :json => @record.errors.to_json, :status => :unprocessable_entity
          end
        rescue Exception => e
          Rails.logger.warn "Unable to update" + e.inspect
          render :plain => "Unable to update record", :status => 500
        end
      end
    end
  end

  private
    def record_params
      params.require(:record).permit(:title, :amount, :date)
    end
end
