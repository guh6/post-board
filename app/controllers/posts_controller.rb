# The HTTP code is new to me. I'm using the information learned from:
# * https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
# * To decide which HTTP code to return 
class PostsController < ApplicationController
  def index
    @posts = Post.all
  end

  # create method creates the post
  #
  # * Created:      201 + JSON entity
  # * Bad Request: 400 + Validation Errors
  # * Error:       500 + Message
  def create
    post = Post.create(post_params)
    if(post.valid?)
      render :json => post.to_json, :status => 201, :content_type => 'application/json'
    else
      response = { :message => 'Bad Request' }
      response.merge!(post.errors.to_h)
      render :json => response.to_json, :status => 400, :content_type => 'application/json'
    end
  rescue Exception => e
    response = { :message => 'Error with server. Cannot create post.' }
    response.merge!( :error => e.message)
    render :json => response.to_json, :status => 500
  end

  # update method updates the post
  #
  # * Updated:     200 + Entity
  # * Bad Request: 412 + Message
  # * Error:       500 + Message
  def update
    post = Post.find(params[:id])
    if (post.update_attributes(post_params))
      render :json => post.to_json, :status => 200, :content_type => 'application/json'
    else
      response = { :message => 'Bad Request for updating' }
      response.merge!(post.errors.to_h)
      render :json => response.to_json, :status => 412, :content_type => 'application/json'
    end
  rescue ActiveRecord::RecordNotFound => e
    render :status => 412
  rescue Exception => e
    response = { :message => 'Server error occurred.' }
    render :status => 500, :json => response.to_json
  end

  # destroy method delets a given post along with any dependencies
  #
  # * Deleted: 200 + Message
  # * Bad Request: 412 + Message
  # * Error: 500 + Message
  def destroy
    post = Post.find(params[:id])
    post.destroy
    if(post.destroyed?)
      render :status => 200, :plain=> 'Record deleted', :content_type => 'text/plain'
    else
      render :status => 412, :plain => 'Cannot delete', :content_type => 'text/plain'
    end
  rescue ActiveRecord::RecordNotFound => e
    render :status => 412, :plain => 'Not found'
  rescue Exception => e
    render :status => 500, :plain => 'Server error'
  end

  private
    def post_params
      params.require(:post).permit(:title, :body, :priority)
    end
end
