class DiscussionsController < ApplicationController

  # The index action (XHR only) retrieves all of the main discussions
  # * follow by the sub-discussions (follow-ups) for each of those.
  #
  # * Returns:
  # * * A JSON object that is hash of { Discussion => [Followups], ..., etc. }
  def index
    begin
    respond_to do |f|
      f.html do
        redirect_to posts_path
      end
      f.js do
        result = {}
        discussions = Discussion.parent_discussions.where(:post_id => params[:post_id])
        if(discussions.nil? || discussions.blank?)
          render :plain => "no content", :status => 204
        else
          discussions.each do |d|
            result[d.to_json] = d.followups.to_json
          end
          Rails.logger.info result
          render :json => result.to_json, :status => 200, :content_type => 'application/json'
        end
      end
    end
    rescue Exception => e
      Rails.logger.info e.inspect
      render :plain => "Server error when retriving discussions and followups", :status => 500
    end
  end

  private
    def discussions_params
      params.require(:discussion).permit(:body)
    end
end
