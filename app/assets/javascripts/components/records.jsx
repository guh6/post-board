// Records ReactComponent contains all of the individual
// record elements.
class Records extends React.Component{

  // Props is from where the view instanitized the react component
  constructor(props){
    super(props);
    this.state = {
      records: this.props.data,
      credit: 1000,
      debit: 0
    }
    this.addRecord = this.addRecord.bind(this);
    this.deleteRecord = this.deleteRecord.bind(this);
    this.updateRecord = this.updateRecord.bind(this);
    this.credits = this.credits.bind(this);
    this.debits = this.debits.bind(this);
    this.balance = this.balance.bind(this);
    this.findRecordsIndex = this.findRecordsIndex.bind(this);
  }

  // If the data property isn't provided, then it is set to this.
  defaultProps(){
    records: []
  }

  addRecord(newRecord){
    this.setState(prevState => ({
      records: prevState.records.concat(newRecord)
    }));
  }

  findRecordsIndex(record){
    var index = -1;
    this.state.records.forEach((item, current_index) => {
      if(item == record)
        index = current_index;
    })
    return index;
  }

  deleteRecord(existingRecord){
    // Find the index
    var index = this.findRecordsIndex(existingRecord);
    var updatedRecords = this.state.records
    // This splice returns the removed array only. So don't use it.
    updatedRecords.splice(index, 1)
    this.setState({records: updatedRecords});
  }

  updateRecord(record, response){
    var index = this.findRecordsIndex(record);
    var updatedRecords = this.state.records;
    updatedRecords[index] = response;
    this.setState({records: updatedRecords});
  }

  //--> AmountBox
  // Sums up all of the records with val >0
  credits(){
    var credits = this.state.records.filter(function(record){
      return record.amount >= 0;
    });
    return credits.reduce(function(sum, record){
      return sum + Number(record.amount);
    }, 0 )
  }

  // Sums up all records with val < 0
  debits(){
    var debits = this.state.records.filter(function(record){
      return record.amount < 0;
    });
    return debits.reduce(function(sum, record){
      return sum + Number(record.amount);
    }, 0 )
  }

  // Sums up credits and debits
  balance(){
    return this.credits() + this.debits();
  }
  //--> End AmountBox

  render(){
    const listRecords = this.state.records.map(function(record){
      return React.createElement(Record, {key: record.id, record: record, handleDeleteRecord: this.deleteRecord, handleEditRecord: this.updateRecord })
    }.bind(this));
    const recordForm = React.createElement(RecordForm, { handleNewRecord: this.addRecord });
    const creditBox =  React.createElement(AmountBox, {amount: this.credits(), text: 'Credit', type:  'success'})
    const debitBox =   React.createElement(AmountBox, {amount: this.debits(), text: 'Debit', type:   'danger'})
    const balanceBox = React.createElement(AmountBox, {amount: this.balance(), text: 'Balance', type: 'info'})

    return(
      <div className = 'records'>
        <h1 className = 'title'> Records </h1>
        <div className='row'>
          <div>{creditBox}</div>
          <div>{debitBox}</div>
          <div>{balanceBox}</div>
        </div>
        <table className = 'table table-bordered'>
          <tbody>
          <tr>
            <th>Date</th>
            <th>Title</th>
            <th>Amount</th>
            <th>Action</th>
          </tr>
          {listRecords}
          </tbody>
        </table>
        {recordForm}
      </div>
    );
  }
}
