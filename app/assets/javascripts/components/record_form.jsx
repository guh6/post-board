// Create an inline form as a ReactForm component
class RecordForm extends React.Component{

  constructor(props){
    super(props);
    this.state = this.initialState();
    // Refer: https://medium.freecodecamp.org/react-binding-patterns-5-approaches-for-handling-this-92c651b5af56
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.valid = this.valid.bind(this);
  }

  initialState(){
    var initialState = {
      title: '',
      date: '',
      amount: '',
      error: '',
      success: '',
    };
    return initialState;
  }

  // Appends the user entered value into the input box and store it as the Component's
  // state
  handleChange(e){
    // Refer: https://facebook.github.io/react/docs/react-component.html#setstate
    var name_of_element = e.target.name
    this.setState({ [name_of_element]: e.target.value})
  }

  valid(){
    return (this.state.title && this.state.amount && this.state.date)
  }

  handleSubmit(e){
    e.preventDefault();
    // Using Jquery UJS only! Regular Jquery will give csrf-token issues:
    payload = {record: this.state}
    $.post( "/records", payload, function( data ) {
      this.setState(this.initialState())
      this.setState({success: 'Added!'})
      this.props.handleNewRecord(data);
    }.bind(this), "JSON").fail(function(data){
      var error_text = "Error: " + data.responseText
      this.setState({success: ''})
      this.setState({error: error_text})
    }.bind(this));
  }

  render(){
    return(
      <div>
        <h2> New Record </h2>
        <h3 className ='error'>   {this.state.error} </h3>
        <h3 className ='success'> {this.state.success} </h3>
        <form className='form-inline' onSubmit={this.handleSubmit}>
          <div className='form-group'>
            <input type='text' className='form-control' placeholder='Date' name='date' value={this.state.date} onChange={this.handleChange}/>
          </div>
          <div className='form-group'>
            <input type='text' className='form-control' placeholder='Title' name='title' value={this.state.title} onChange={this.handleChange}/>
          </div>
          <div className='form-group'>
            <input type='text' className='form-control' placeholder='Amount' name='amount' value={this.state.amount} onChange={this.handleChange}/>
          </div>
          <div className='form-group'>
            <button type='submit' className='btn btn-primary' disabled = {!this.valid()}> Create record</button>
          </div>
        </form>
      </div>
    )
  }
}
