// Record Reacton component
class Record extends React.Component{
  // For debugging purproses
  constructor(props){
    super(props);
    this.state = { edit: false };
    this.handleDelete = this.handleDelete.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
    this.recordRow = this.recordRow.bind(this);
    this.recordForm = this.recordForm.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
  }

  handleDelete(e){
    e.preventDefault();
    var url = "/records/" + this.props.record.id
    $.ajax({ url: url,
             type: 'DELETE'}).success(function(data){
               this.props.handleDeleteRecord(this.props.record);
             }.bind(this)).fail(function(data){
               alert("Record not deleted" + data);
             });
    return;
  }

  handleEdit(e){
    e.preventDefault();
    var url = "/records/" + this.props.record.id
    var payload = {
      title: ReactDOM.findDOMNode(this.refs.title).value,
      date:  ReactDOM.findDOMNode(this.refs.date).value,
      amount: ReactDOM.findDOMNode(this.refs.amount).value
    };
    $.ajax({ url: url,
             type: 'PUT',
             data: {record: payload},
             dataType: 'JSON' }).success(function(data){
              //  Deal with success
              this.setState({edit: false})
              this.props.handleEditRecord(this.props.record, data);
             }.bind(this)).fail(function(data){
              //  Deal with failure
              alert("Record not updated");
            });
  }

  handleToggle(e){
    e.preventDefault();
    this.setState({edit: !this.state.edit})
  }

  recordRow(){
    return(
      <tr >
        <td className = 'date'> {this.props.record.date}</td>
        <td className = 'title'> {this.props.record.title}</td>
        <td className = 'Amount'> {amountFormat(this.props.record.amount)} </td>
        <td className = 'Action'>
          <a className ='btn btn-danger' onClick={this.handleDelete}>Delete</a>
          <a className ='btn btn-default' onClick={this.handleToggle}>Edit</a>
        </td>
      </tr>
    );
  }

  recordForm(){

    return(
      <tr >
        <td className = 'date'>
          <input className = 'form-control' type='text' defaultValue={this.props.record.date} ref='date'/>
        </td>
        <td className = 'title'>
          <input className = 'form-control' type='text' defaultValue={this.props.record.title} ref='title'/>
        </td>
        <td className = 'Amount'>
          <input className = 'form-control' type='text' defaultValue={this.props.record.amount} ref='amount'/>
        </td>
        <td className = 'Action'>
          <a className ='btn btn-danger' onClick={this.handleDelete}>Delete</a>
          <a className ='btn btn-default' onClick={this.handleEdit}>Update</a>
          <a className ='btn btn-default' onClick={this.handleToggle}>Cancel</a>
        </td>
      </tr>
    );
  }

  render(){
    if(this.state.edit)
      return this.recordForm();
    else
      return this.recordRow();
  }
}
