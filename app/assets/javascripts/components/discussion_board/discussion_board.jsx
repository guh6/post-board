// handles when a new group is added (aka main discussion thread)
//  handles when a existing group is deleted from group
class DiscussionBoard extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      discussions: {},
      edit: false,
      completed: false
    }
    console.log('DiscussionBoard recieved: ' + JSON.stringify(props));
    this.getDiscussions = this.getDiscussions.bind(this);
    this.expandableBoard = this.expandableBoard.bind(this);
  }

  componentDidMount(){
    console.log("HEllo");
    this.getDiscussions();
  }

  getDiscussions(){
    console.log("Getting discussions for: " + this.props.post.id);
    var url = "/posts/" + this.props.post.id +"/discussions"
    this.setState({completed: false})
    $.ajax( { url: url,
              type: 'GET',
              dataType: 'JSON'}).success(function(data){
                console.log("Success");
                if(data){
                  this.props.discussions = data
                }
                this.setState({discussions: data, completed: true})

              }.bind(this)).fail(function(data){
                this.props.discussions = data
                this.setState({discussions: {}, completed: true})
                console.log("Failed");
                console.log(data);
              });
    return;
  }

  expandableBoard(){
    // Groups contain a DiscussionGroup element
    var groups = [];
    for(var parent in this.state.discussions)
    {
      var parent_obj = JSON.parse(parent);
      var children_obj = JSON.parse(this.state.discussions[parent]);
      groups.push(React.createElement(DiscussionGroup, {key: parent_obj.id, parent: parent_obj, children: children_obj}));
    }
    return(
      <div className = 'container-fluid'>
        <h2 className='title'> Discussions </h2>
          <div className ='row'>
            <div className = 'col'> ADD NEW ACTION </div>
          </div>
        {groups}
      </div>
    );
  }

  render(){
    if(this.state.completed) {
      return this.expandableBoard();
    }
    else {
      return null;
    }
  }
}
