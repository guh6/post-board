// Each group is main d and child d.
//  Allows - delete and reply to the group
//
//
class DiscussionGroup extends React.Component{
  constructor(props){
    super(props);
    console.log("D-Group received: " +JSON.stringify(props));
  }

  childRows(children){
    if(children.length == 0 )
      return null;
    else {
      var elements = children.map((child) => {
        return React.createElement(Discussion, {key: child.id, discussion: child})
      });
      return elements;
    }
  }

  render(){
    const parentDiscussion = React.createElement(Discussion, {discussion: this.props.parent});
    const childDiscussions = this.childRows(this.props.children);

    // Display parent
    // Display children
    return (
      <div className='discussionBoard'>
        <table className='table table-bordered'>
          <tbody>
            {parentDiscussion}
            {childDiscussions}
          </tbody>
        </table>
      </div>
    );
  }
}
