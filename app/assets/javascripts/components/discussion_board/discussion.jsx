// Displays individual discussion
// allows delete of single discussion, edit of single dicussion

class Discussion extends React.Component{
  constructor(props){
    super(props);
    console.log("Discussion received: " + JSON.stringify(props));
    this.parentRow = this.parentRow.bind(this);
    this.childRow = this.childRow.bind(this);
  }


  isParent(parent_id){
    if(parent_id)
      return false
    else
      return true
  }

  parentRow(){
    const discussion = this.props.discussion;
    return (
      <tr>
        <td className='parent-cell'>{discussion.body}</td>
        <td> Reply </td>
        <td> Edit </td>
        <td> Delete </td>
      </tr>
    );
  }

  childRow(){
    const discussion = this.props.discussion;
    return(
      <tr>
        <td className='child-cell'>{discussion.body}</td>
        <td></td>
        <td> Edit </td>
        <td> Delete </td>
      </tr>
    )
  }

  render(){
    if(this.isParent(this.props.discussion.parent_id)){
      return this.parentRow();
    }
    else
      return this.childRow();
  }
}
