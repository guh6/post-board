this.amountFormat = function(amount){
  return ('$' + Number(amount).toLocaleString());
};

this.readableTime = function(time){
  dateTime = new Date(time);
  dateString = dateTime.toLocaleDateString();
  timeString = dateTime.toLocaleTimeString();
  return(dateString + ", " + timeString);
}
