class Discussion < ApplicationRecord
  belongs_to :post

  belongs_to :parent, :class_name => 'Discussion', :optional => true

  has_many :followups, :class_name => 'Discussion', :foreign_key => 'parent_id'

  validates :body, :presence => { message: 'Must include body' }

  scope :parent_discussions, lambda{ where(:parent_id => nil) }

  scope :recently_created, lambda { order("created_at DESC") }

end
