class Post < ApplicationRecord

  enum :priority =>  [:low, :normal, :high] #Post.priorities => {"low"=>0, "normal"=>1, "high"=>2}

  has_many :discussions

  validates :title, :presence => {:message => "Must provide a title"}
  validates :body,  :presence => {:message => "Must provide a body"}
  validates :priority,  :presence => {:message => "Must provide a priority"}

   scope :recently_updated , lambda{ order('updated_at DESC') }
end
