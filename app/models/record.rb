class Record < ApplicationRecord
  validates :title, :presence => true
  validates :amount, :presence => true
  validates :date, :presence => true
end
