# Post Board example:


# (August, 2017): To gain better exposure with React and Rails, I have decided to create a Single page Discussion board where we have a Post and Discussion resource.

  - As a single app, I can create Post with title, body and priority.
  - As a post, the priorities are low, normal and high.
  - As a single app, I can post a discussion to a post with body that belongs to the post.
  - As a post, I can have many discussions but they are all within a single post
  - As a discussion, I can have at most one child discussion to have a discussion on a discussion.  
  - As a post, I can edit and delete myself.
  - As a post, when I delete myself, all discussions are deleted.
  - As a discussion I can edit, and delete my post.
  - As a discussion, when I delete myself, it is only allowable if I do not have child discussions.
  - Any changes to discussion updates the post updated_at date including edit, delete and new.


# Learning expectation

## With this example, I intend to follow TDD for any RoR components.

## I intend to replace the view in RoR MVC with React.js

## The order of tests are in the order of importance:

  1. Models
  2. Helpers
  3. Controllers
  4. Views - React ! How?

# TODO Learnings:

## User Authentication - implement authentication layer ontop of post/discussion.

  1. Session Authentication

  2. Token Authentication

## User Registration

## React.js Modals
