class AddPostToDiscussions < ActiveRecord::Migration[5.1]
  def change
    add_reference :discussions, :post, foreign_key: true
  end
end
