class RemoveForeignKeyFromPosts < ActiveRecord::Migration[5.1]
  def change
    remove_column :posts, :remove_column, :foreign_key
  end
end
