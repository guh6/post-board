class CreateDiscussions < ActiveRecord::Migration[5.1]
  def change
    create_table :discussions do |t|
      t.text :body
      t.timestamps

      t.references :posts
    end

  end
end
