class CreatePosts < ActiveRecord::Migration[5.1]
  def change
    create_table :posts do |t|
      t.string :title
      t.text :body
      t.integer :parent_id
      t.integer :priority

      t.timestamps
    end
  end
end
