class RemovePostsIdFromDiscussions < ActiveRecord::Migration[5.1]
  def change
    remove_column :discussions, :posts_id, :integer
  end

end
