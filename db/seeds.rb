# Seed Records
(0..10).each do |i|
  Record.create do |r|
    r.title = Faker::RockBand.name
    r.amount = i.even? ? (100+i*2) : (10-i*5)
    r.date = Faker::Date.between(1.years.ago, Date.today)
  end
end

# Seed Posts and Discussions
(0..5).each do |i|
  main_post = Post.create do |p|
    p.title = Faker::HitchhikersGuideToTheGalaxy.starship
    p.body = Faker::HitchhikersGuideToTheGalaxy.quote
    p.priority = i % (Post.priorities.count - 1)
  end
  updated = Faker::Date.between(1.years.ago, Date.today)
  created = Faker::Date.between(1.years.ago, updated)
  main_post.update_attributes(:updated_at => updated, :created_at => created )

  (1...5).each do |d|
    main_discussion = Discussion.create(:body => Faker::Lorem.paragraph, :post=>main_post)

    max_child = 10 % (i+d)
    (1..max_child).each  do
      Discussion.create(:body => Faker::Simpsons.quote, :post=>main_post, :parent_id => main_discussion.id)
    end

  end
end
